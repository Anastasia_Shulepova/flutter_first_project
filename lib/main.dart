import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'domain/providers.dart';
import 'ui/app_theme.dart';
import 'domain/logger.dart';

import 'ui/screens/main_page.dart';

void main() {
  initLogger();
  logger.info('Start main');
  runApp(
    const ProviderScope(
      child: MyHomePage(),
    ),
  );
}

class MyHomePage extends ConsumerStatefulWidget {
  const MyHomePage({super.key});

  @override
  ConsumerState<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends ConsumerState<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    var news = ref.read(apiProvider).getNews();
    final isDark = ref.watch(themeProvider);
    return MaterialApp(
      theme: AppTheme.theme(isDark),
      home: MainPage(news),
    );
  }
}
