import 'dart:convert';
import 'package:http/http.dart' as http;
import '../data/news_dto.dart';
import 'logger.dart';

const apiKey = 'f12f5c958cbd40b1bde28a1f68a67fe4';

class Api {
  http.Client client;
  Api(this.client);
  Future<List<NewsDTO>> getNews() async {
    Uri uri =
        Uri.parse('https://newsapi.org/v2/everything?q=animals&apiKey=$apiKey');

    final response = await client.get(uri);
    logger.info('get news ${response.statusCode}');

    if (response.statusCode == 201 || response.statusCode == 200) {
      List articles = json.decode(response.body)['articles'];
      List<NewsDTO> news = articles.map((x) => NewsDTO.fromJson(x)).toList();
      return news;
    } else {
      return [];
    }
  }
}
