import 'package:localstorage/localstorage.dart';

import '../data/news_dto.dart';
import 'logger.dart';

class LocalStorageService {
  LocalStorage storage;
  LocalStorageService(this.storage);
  void addFavouriteNews(NewsDTO model) async {
    await storage.ready;
    if (storage.getItem('favourite_news') != null) {
      logger.info('add favourite new with title ', model.title);
      List<dynamic> items = storage.getItem('favourite_news');
      if (!items
          .map((item) => NewsDTO.fromJson(item).hashCode)
          .contains(model.hashCode)) {
        items.add(model.toJson());
        storage.setItem('favourite_news', items);
      }
    } else {
      List<dynamic> items = [];
      items.add(model.toJson());
      storage.setItem('favourite_news', items);
    }
  }

  Future<List<NewsDTO>> getFavouriteNews() async {
    await storage.ready;
    List articles = storage.getItem('favourite_news') ?? List.empty();
    return articles.map((e) => NewsDTO.fromJson(e)).toList();
  }
}
