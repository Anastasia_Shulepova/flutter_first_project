import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:localstorage/localstorage.dart';

import 'api.dart';
import 'local_storage_service.dart';
import 'package:http/http.dart' as http;

final apiProvider = Provider((ref) => Api(http.Client()));
final themeProvider = StateProvider<bool>((ref) => false);

final localStorageProvider =
    Provider((ref) => LocalStorage('favourite_news.json'));
final localStorageServiceProvider =
    Provider((ref) => LocalStorageService(ref.read(localStorageProvider)));
