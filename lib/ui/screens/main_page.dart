import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../data/news_dto.dart';
import '../../domain/providers.dart';
import 'favourite_news_list.dart';
import '../../domain/logger.dart';
import '../widgets/news_card.dart';

class MainPage extends ConsumerWidget {
  final Future<List<NewsDTO>> news;

  const MainPage(this.news, {super.key});

  @override
  Widget build(BuildContext context, ref) {
    final isDark = ref.watch(themeProvider);
    return Scaffold(
        appBar: AppBar(
          title: const Text('Pig.Press'),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              logger.info('open window with favourite news');
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const FavouriteNewsList();
              }));
            },
            icon: const Icon(
              Icons.favorite,
              size: 35,
            ),
          ),
          actions: [
            IconButton(
              onPressed: () {
                logger.info('change theme');
                ref.read(themeProvider.notifier).state = !isDark;
              },
              icon: isDark
                  ? Image.asset('assets/pig.png')
                  : Image.asset('assets/pig_light.png'),
            )
          ],
        ),
        body: Center(
            child: FutureBuilder(
          future: news,
          builder: (context, news) {
            if (!news.hasData) {
              logger.info('no news');
              return const Center(child: Text('sorry, no news yet'));
            } else {
              logger.info('add list with news');
              return ListView.builder(
                itemCount: news.data?.length,
                itemBuilder: (context, index) => NewsCard(
                  title: news.data!.elementAt(index).title,
                  author: news.data!.elementAt(index).author,
                  imageUrl: news.data!.elementAt(index).urlToImage,
                  description: news.data!.elementAt(index).description,
                  content: news.data!.elementAt(index).content,
                  url: news.data!.elementAt(index).url,
                ),
              );
            }
          },
        )));
  }
}
