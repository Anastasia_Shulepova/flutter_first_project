import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../data/news_dto.dart';
import '../../domain/providers.dart';
import '../../domain/logger.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void _launchURL(url) async {
  final Uri url0 = Uri.parse('$url');
  if (!await launchUrl(url0)) {
    throw Exception('Could not launch $url0');
  }
}

class DetailedNew extends ConsumerWidget {
  final String title;
  final String imageUrl;
  final String content;
  final String url;
  final String author;
  final String description;

  const DetailedNew(
      {required this.title,
      required this.imageUrl,
      required this.content,
      required this.url,
      required this.author,
      required this.description,
      super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isDark = ref.watch(themeProvider);
    return Scaffold(
        appBar: AppBar(
          title: const Text('Pig.Press'),
          centerTitle: true,
          actions: [
            IconButton(
              onPressed: () {
                logger.info('change theme');
                ref.read(themeProvider.notifier).state = !isDark;
              },
              icon: isDark
                  ? Image.asset('assets/pig.png')
                  : Image.asset('assets/pig_light.png'),
            )
          ],
        ),
        body: Center(
          child: Container(
              margin: const EdgeInsets.all(14),
              padding: const EdgeInsets.all(14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 15),
                    child: Text(
                      title,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          flex: 7,
                          child: ElevatedButton(
                            onPressed: () {
                              logger.info('go to the web with new $title');
                              _launchURL(url);
                            },
                            child: const Text('Go To Website'),
                          )),
                      Expanded(
                        flex: 5,
                        child: Text(
                          author,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.titleSmall,
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Expanded(
                          flex: 2,
                          child: SizedBox(
                              width: 30,
                              height: 30,
                              child: ElevatedButton(
                                  onPressed: () => {
                                        ref
                                            .read(localStorageServiceProvider)
                                            .addFavouriteNews(NewsDTO(
                                                title,
                                                description,
                                                imageUrl,
                                                author,
                                                content,
                                                url))
                                      },
                                  style: ElevatedButton.styleFrom(
                                    shape: const CircleBorder(),
                                    padding: const EdgeInsets.all(0),
                                  ),
                                  child: const Icon(
                                    Icons.favorite,
                                    size: 20,
                                  ))))
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 15),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(
                          imageUrl,
                          fit: BoxFit.cover,
                          width: double.infinity,
                          errorBuilder: (context, error, trace) {
                            return const SizedBox(
                                height: 175,
                                child: Center(child: Text('No Image')));
                          },
                        )),
                  ),
                  Text(content, style: Theme.of(context).textTheme.titleMedium),
                ],
              )),
        ));
  }
}
