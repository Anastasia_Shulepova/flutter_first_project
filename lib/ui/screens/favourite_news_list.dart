import 'package:flutter/material.dart';
import 'package:news/ui/widgets/favourite_new.dart';
import '../../data/news_dto.dart';
import '../../domain/providers.dart';
import '../../domain/logger.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class FavouriteNewsList extends ConsumerWidget {
  const FavouriteNewsList({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isDark = ref.watch(themeProvider);
    Future<List<NewsDTO>> news =
        ref.read(localStorageServiceProvider).getFavouriteNews();
    return Scaffold(
        appBar: AppBar(
          title: const Text('Pig.Favourite'),
          centerTitle: true,
          actions: [
            IconButton(
              onPressed: () {
                logger.info('change theme');
                ref.read(themeProvider.notifier).state = !isDark;
              },
              icon: isDark
                  ? Image.asset('assets/pig.png')
                  : Image.asset('assets/pig_light.png'),
            )
          ],
        ),
        body: Center(
            child: FutureBuilder(
                future: news,
                builder: (context, news) {
                  if (!news.hasData) {
                    logger.info('no news');
                    return const Center(child: Text('sorry, no news yet'));
                  }
                  return ListView.builder(
                    itemCount: news.data?.length,
                    itemBuilder: (context, index) => FavouriteNew(
                      title: news.data!
                          .elementAt(news.data!.length - index - 1)
                          .title,
                      author: news.data!
                          .elementAt(news.data!.length - index - 1)
                          .author,
                      imageUrl: news.data!
                          .elementAt(news.data!.length - index - 1)
                          .urlToImage,
                      description: news.data!
                          .elementAt(news.data!.length - index - 1)
                          .description,
                      content: news.data!
                          .elementAt(news.data!.length - index - 1)
                          .content,
                      url: news.data!
                          .elementAt(news.data!.length - index - 1)
                          .url,
                    ),
                  );
                })));
  }
}
