class NewsDTO {
  String title, description, urlToImage, author, content, url;

  NewsDTO(this.title, this.description, this.urlToImage, this.author,
      this.content, this.url);

  factory NewsDTO.fromJson(Map<String, dynamic> json) {
    return NewsDTO(
        json['title'] ?? '',
        json['description'] ?? '',
        json['urlToImage'] ?? 'No Image',
        json['author'] ?? '',
        json['content'] ?? '',
        json['url'] ?? '');
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'description': description,
      'urlToImage': urlToImage,
      'author': author,
      'content': content,
      'url': url,
    };
  }

  @override
  bool operator ==(Object other) {
    if (other is NewsDTO &&
        other.title == title &&
        other.description == description &&
        other.urlToImage == urlToImage &&
        other.author == author &&
        other.url == url) {
      return true;
    }
    return false;
  }

  @override
  int get hashCode => (title + description).hashCode;
}
