import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:news/data/news_dto.dart';
import 'package:news/domain/api.dart';
import 'get_news_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  group('getNewsTest', () {
    test('returns a list with new if the http call completes successfully',
        () async {
      final client = MockClient();
      Uri uri = Uri.parse(
          'https://newsapi.org/v2/everything?q=animals&apiKey=$apiKey');
      when(client.get(uri)).thenAnswer((_) async => http.Response(
          '{"articles": [{"title": "a", "description": "b", "urlToImage": "img", "author": "c", "content": "dd", "url": "u"}]}',
          200));
      List<NewsDTO> news = [];
      news.add(NewsDTO('a', 'b', 'img', 'c', 'dd', 'u'));
      expect(await Api(client).getNews(), news);
    });

    test('returns an empty list if the http call completes with an error',
        () async {
      final client = MockClient();
      Uri uri = Uri.parse(
          'https://newsapi.org/v2/everything?q=animals&apiKey=$apiKey');
      when(client.get(uri))
          .thenAnswer((_) async => http.Response('Not Found', 404));

      expect(await Api(client).getNews(), []);
    });
  });
}
